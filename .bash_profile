export PS1="\[\033[0;92m\]\u\[\033[0;92m\]: \W$ "

## Homebrew
export PATH="/usr/local/bin:$PATH"
export PATH="/usr/local/sbin:$PATH"

## Homebrew bash completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
  source $(brew --prefix)/etc/bash_completion
fi

syntasa_site_maps(){
  curl -L http://www.syntasa.com/hgr_portfolio-sitemap.xml |  awk 'BEGIN{FS="<loc>";} {print $2}' | awk 'BEGIN{FS="</loc>"} {print $1}'
}

#### COLOR TEST

color_test(){
  T='gYw'   # The test text

echo -e "\n                 40m     41m     42m     43m\
     44m     45m     46m     47m";

for FGs in '    m' '   1m' '  30m' '1;30m' '  31m' '1;31m' '  32m' \
           '1;32m' '  33m' '1;33m' '  34m' '1;34m' '  35m' '1;35m' \
           '  36m' '1;36m' '  37m' '1;37m';
  do FG=${FGs// /}
  echo -en " $FGs \033[$FG  $T  "
  for BG in 40m 41m 42m 43m 44m 45m 46m 47m;
    do echo -en "$EINS \033[$FG\033[$BG  $T  \033[0m";
  done
  echo;
done
echo
}

#### IMAGE COMPRESSION

image_compress() {
  if [ "$8" == "" ];then
    printf "> Usage: Please specify all parameters \n\n> Example: image_compress -quality 85 -size 1920 -file_type jpg -dir /some_folder\n\n"
    kill -INT $$
  fi

  quality=$2
  maxWidth=$4
  extension=$6
  directory=$8

  mogrify -strip -interlace Plane -gaussian-blur 0.05 -quality $quality% -resize "$maxWidth>" "$directory/*.$extension"
  printf "Image compression complete \n\n"
}

#### SERVER ACCESS

alias server.siteground="ssh joshuauz@joshuauzzell.com -p 18765"
alias server.sharkvm="ssh ec2-user@ec2-52-22-13-53.compute-1.amazonaws.com"

#### GLOBAL SERVER VARIABLES

UserName="$(whoami)"
sites_folder=/Users/$UserName/Sites

nginx_conf=/usr/local/etc/nginx/nginx.conf
nginx_vhost_logs=$sites_folder/logs/nginx
nginx_servers=/usr/local/etc/nginx/servers

apache_conf=/etc/apache2/httpd.conf
apache_vhost_logs=$sites_folder/logs/apache
apache_servers=/etc/apache2/extra/available-hosts

#############

alias profile.update="source ~/.bash_profile && echo 'profile updated'"
alias profile="sudo nano ~/.bash_profile && profile.update"

### Traversing Directory
alias .="cd ."
alias ..="cd .."

alias ll='ls -lahF'

# Change dir and list content
leer(){
  if [ "$1" == "" ];then
    printf "> %s" "Usage: leer <file/path>"
    kill -INT $$
  fi
  cd $1 &&  ll
}

permit(){
  read -p "Are you sure? [y] or [n]: " response
  echo $response
}

### Monitoring Network Connections
alias ip4="lsof -Pn -i4 | grep 'LISTEN'"
alias ip6="lsof -Pn -i6"
alias listening='netstat -an tcp | grep -i "LISTEN"'

pid.search(){
  if [ "$1" == "" ];then
    printf "> $1%s" "Usage: pid.search <process name>"
    kill -INT $$
  fi
  ps -ax | grep $1
}
pid.kill(){
  if [ "$1" == "" ];then
    printf "> $1%s" "Usage: pid.kill <process id>"
    kill -INT $$
  fi
  kill $1
}

### Homebrew Helpers
alias brewed='brew services list'


### LEMP Server Management
ramp.permit(){
  read -p "Are you sure? [y] or [n]: " response
  echo $response
}
ramp.help(){
  declare -F | grep 'ramp' | awk '{printf "> %s\n" , $3}'
}
ramp.status(){
  printf "\n> Apache - 1270.0.0.1:8080\n"
  curl -IL http://127.0.0.1:8080
  printf "\n> Nginx - 127.0.0.1:80\n"
  curl -IL http://127.0.0.1:80
  printf "\n> MySQL - 127.0.0.1:3306\n"
  curl -IL http://127.0.0.1:3306
  printf "\n==\n\n"
  listening
  printf "\n"
}
ramp.up(){
  cd /Users/sam/Sites/
  printf "Ramping Up \n+++++++++++++++\n"
  sudo apachectl start
  sudo nginx
  mysql.server start
  printf "\n==\n"
  printf "\n"
  printf " _______        _____        _______ \n"
  printf "|   __   \    /       \    /     _   \ \n"
  printf "|  |  |  |   /  /      \  /     |     | \n"
  printf "|  |  |  |  /  /__  |\  \/  /|   ___ / \n"
  printf "|  | \  \__/  ___   | \    / |  |  \n"
  printf "|__|  \ ____ /   |__|  \__/  |__| up \n"
  printf "\n"
  ramp.status
  ramp.hosts
}

ramp.down(){
  printf "Ramping Down \n+++++++++++++++\n"
  sudo apachectl stop
  sudo nginx -s stop
  mysql.server stop
  printf "\n==\n"
  printf "\n"
  printf " _______        _____        _______ \n"
  printf "|   __   \    /       \    /     _   \ \n"
  printf "|  |  |  |   /  /      \  /     |     | \n"
  printf "|  |  |  |  /  /__  |\  \/  /|   ___ / \n"
  printf "|  | \  \__/  ___   | \    / |  |  \n"
  printf "|__|  \ ____ /   |__|  \__/  |__| down \n"
  printf "\n"
  ramp.status
}
ramp.restart(){
  ramp.down
  ramp.up
}

ramp.createNginxConf(){
url=$1.localhost
sudo cat <<EOF > $nginx_servers/$1.conf
upstream $url {
  server 127.0.0.1:8080;
}
server {
  listen *:80;
  server_name $url;

  access_log $nginx_vhost_logs/$1/access.log;
  error_log $nginx_vhost_logs/$1/error.log;

  location / {
    proxy_pass http://$url;
  }
}
EOF
}

ramp.createApacheConf(){
cat <<EOF > $apache_servers/$1.conf
<VirtualHost *:8080>
    ServerName $1.localhost
    DocumentRoot "$sites_folder/$2"
    ErrorLog "$apache_vhost_logs/$1/error_log"
    CustomLog "$apache_vhost_logs/$1/access_log" common
</VirtualHost>
EOF
}

ramp.updateEtcHosts(){
url=$1.localhost
cat <<EOF >>  /etc/hosts
127.0.0.1       $url
::1             $url

EOF
}

ramp.addhost(){
  url=$1.localhost
  if [ "$1" !=  "" ] && [ "$2" != "" ];then
    ramp.createNginxConf $1
    ramp.createApacheConf $1 $2
    ramp.updateEtcHosts $1
    awk '{print}'  $nginx_servers/$1.conf $apache_servers/$1.conf /etc/hosts
    mkdir $apache_vhost_logs/$1
    mkdir $nginx_vhost_logs/$1
    printf "\n==========\n"
    ramp.restart
    printf "\nRamp Rolling!!!! \n"
  else
    printf "> Usage: ramp.addhost <sitename> <Sites/path/to/public-folder>\n"
  fi
}

ramp.hosts(){
  printf "\nHosts file\n=============================\n"
  printf "> /etc/hosts\n\n"
  cat /etc/hosts
  printf "\nnGinx\n=============================\n"
  printf "> $nginx_servers\n\n"
  ll $nginx_servers
  printf "\nApache\n============================\n"
  printf "> $apache_servers\n\n"
  ll $apache_servers
  printf "\n\n"
}
alias ramp.host=ramp.hosts

ramp.folder(){
  if [ "$1" == "" ];then
    printf "> \n"
    kill -INT $$
  fi
  if [ "$1" == "sites" ];then
    cd $sites_folder && ll
  elif [ "$1" == "logs" ];then
    cd $sites_folder/logs && ll
  fi
}

ramp.log(){
  use="> Usage: ramp.log <{atom, nano or cat}> <web server> <hostname> <{error or access}>"
  if [ "$1" != "atom" ] && [ "$1" != "nano" ] && [ "$1" != "cat" ];then
    printf "$use\n"
    kill -INT $$
  fi
  if [ "$2" == "nginx" ];then
    if [ "$3" != "" ] && [ "$4" != "" ];then
      echo ">> Notice: Displaying $4 log for nginx"
      $1 $nginx_vhost_logs/$3/$4.log
    else
      printf "$use\n"
    fi
  elif [ "$2" == "apache" ];then
    if [ "$3" != "" ] && [ "$4" != "" ];then
      echo ">> Notice: Displaying $4 log for apache"
      $1 $apache_vhost_logs/$3/$4_log
    else
     printf "$use\n"
    fi
  else
   printf "$use\n"
  fi
}

ramp.edit(){
  use="> Usage: ramp.edit <{atom or nano}> <web server> <hostname (optional)>"
  if [ "$1" != "atom" ] && [ "$1" != "nano" ];then
    printf "$use\n"
    kill -INT $$
  fi
  if [ "$2" == "nginx" ];then
    if [ "$3" != "" ];then
      $1 $nginx_servers/$3.conf
    else
      $1 $nginx_conf
    fi
  elif [ "$2" == "apache" ];then
    if [ "$3" != "" ];then
      $1 $apache_servers/$3.conf
    else
      $1 $apache_conf
    fi
  elif [ "$2" == "host" ] || [ "$2" == "hosts" ];then
    sudo $1 /etc/hosts
  else
    printf "$use\n"
  fi
}

ramp.removehost(){
  rm /etc/apache2/extra/available-hosts/$1.conf
  rm /usr/local/etc/nginx/servers/$1.conf
  sudo nano /etc/hosts
}

